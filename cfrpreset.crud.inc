<?php
use Drupal\cfrapi\SummaryBuilder\SummaryBuilder_Static;

/**
 * @param string $interface
 * @param string $machine_name
 *
 * @return \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|object
 */
function _cfrpreset_create_stub($interface, $machine_name) {
  /** @var object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record */
  $record = new \stdClass();
  $record->id = NULL;
  $record->interface = $interface;
  $record->machine_name = $machine_name;
  $record->identifier = $interface . '|' . $machine_name;
  $record->label = '';
  $record->plugin_id = NULL;
  $record->plugin_options = NULL;
  return $record;
}

/**
 * @param \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|object $record
 *
 * @return array
 */
function _cfrpreset_preset_get_plugin_conf($record) {
  return [
    'id' => $record->plugin_id,
    'options' => $record->plugin_options,
  ];
}

/**
 * @param \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|object $record
 *
 * @return array
 */
function _cfrpreset_preset_find_modifications($record) {
  $record_current = _cfrpreset_load_preset_by_identifier($record->identifier);
  $modified = [];
  foreach ($record_current as $k => $v) {
    if (!property_exists($record, $k)) {
      $modified[$k] = $v;
    }
    elseif ($record->$k !== $v) {
      $modified[$k] = $v;
    }
  }
  // Don't warn for type = Overridden.
  unset($modified['type']);
  return $modified;
}

/**
 * @param \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|object $record
 *
 * @return string
 */
function _cfrpreset_preset_get_plugin_summary($record) {
  $conf = _cfrpreset_preset_get_plugin_conf($record);
  $summary = cfrplugin()->interfaceGetConfigurator($record->interface)->confGetSummary($conf, new SummaryBuilder_Static());
  return render($summary);
}

/**
 * @param string $interface
 *
 * @return object[]|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface[]
 */
function _cfrpreset_interface_get_presets_by_machine_name($interface) {
  $records = _cfrpreset_load_presets_by_conditions(
    [
      'interface' => $interface,
    ]
  );

  $records_by_machine_name = [];
  foreach ($records as $record) {
    $records_by_machine_name[$record->machine_name] = $record;
  }
  return $records_by_machine_name;
}

/**
 * @return object[][]|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface[][]
 */
function _cfrpreset_get_presets_by_interface_and_machine_name() {
  ctools_include('export');
  $records = ctools_export_load_object(CFRPRESET_TABLE_NAME);

  $records_grouped = [];
  foreach ($records as $record) {
    $records_grouped[$record->interface][$record->machine_name] = $record;
  }
  return $records_grouped;
}

/**
 * @param string $interface
 * @param string $machine_name
 *
 * @return object|null|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface
 */
function _cfrpreset_load_preset($interface, $machine_name) {
  return _cfrpreset_load_preset_by_conditions(
    [
      'interface' => $interface,
      'machine_name' => $machine_name,
    ]
  );
}

/**
 * @param int $id
 *
 * @return \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|null|object
 */
function _cfrpreset_load_preset_by_id($id) {
  return _cfrpreset_load_preset_by_conditions(
    [
      'id' => $id,
    ]
  );
}

/**
 * @param string $identifier
 *
 * @return object|null|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface
 */
function _cfrpreset_load_preset_by_identifier($identifier) {
  return _cfrpreset_load_preset_by_conditions(
    [
      'identifier' => $identifier,
    ]
  );
}

/**
 * @param string[] $conditions
 *
 * @return object|null|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface
 */
function _cfrpreset_load_preset_by_conditions(array $conditions) {
  $records = _cfrpreset_load_presets_by_conditions($conditions);
  return array_shift($records);
}

/**
 * @param array $conditions
 *
 * @return object[]|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface[]
 */
function _cfrpreset_load_presets_by_conditions(array $conditions) {
  ctools_include('export');
  return ctools_export_load_object(CFRPRESET_TABLE_NAME, 'conditions', $conditions);
}

/**
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return bool|int
 *   FALSE or SAVED_NEW or SAVED_UPDATED
 */
function _cfrpreset_preset_save($record) {

  if (isset($record->export_type) && $record->export_type & EXPORT_IN_DATABASE) {
    // Existing record.
    $update = ['identifier'];
  }
  else {
    // New record.
    $update = [];
    $record->export_type = EXPORT_IN_DATABASE;
  }

  $result = drupal_write_record(CFRPRESET_TABLE_NAME, $record, $update);

  cache_clear_all(CFRPRESET_CID_PRESETS_ALL, CFRPRESET_CACHE_BIN);
  cache_clear_all('cfrplugin:', 'cache', TRUE);

  return $result;
}

/**
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 * @param bool $ctools_crud
 */
function _cfrpreset_preset_export_delete($record, $ctools_crud = TRUE) {

  $query = db_delete(CFRPRESET_TABLE_NAME);

  if (isset($record->identifier)) {
    $query->condition('identifier', $record->identifier);
    if (!$ctools_crud) {
      ctools_export_crud_disable(CFRPRESET_TABLE_NAME, $record->identifier);
    }
  }
  elseif (isset($record->id)) {
    $query->condition('id', $record->id);
  }

  /** @noinspection IsEmptyFunctionUsageInspection */
  if (!empty($record->view_mode)) {
    $query->condition('view_mode', $record->view_mode);
  }

  $query->execute();

  cache_clear_all(CFRPRESET_CID_PRESETS_ALL, CFRPRESET_CACHE_BIN);
  cache_clear_all('cfrplugin:', 'cache', TRUE);
  cache_clear_all('cfrrealm:', 'cache', TRUE);
}
