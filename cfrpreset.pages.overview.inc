<?php

/**
 * @return array
 *   A render array.
 */
function _cfrpreset_overview_page() {

  $records_grouped = _cfrpreset_get_presets_by_interface_and_machine_name();

  $rows = [];
  foreach (cfrplugin()->getInterfaceLabels() as $interface => $interfaceLabel) {

    $interface_path = _cfrpreset_interface_get_path($interface);

    $presets_html = '';
    if (array_key_exists($interface, $records_grouped)) {
      $interface_records = $records_grouped[$interface];
      foreach ($interface_records as $machine_name => $record) {
        $preset_path = _cfrpreset_preset_get_path($record);
        $presets_html .= ''
          . '<li>'
          . l($record->label, $preset_path)
          . '</li>';
      }
    }

    $add_preset_link = l(t('add preset'), $interface_path . '/add');

    if ('' !== $presets_html) {
      $presets_html = '<ul>' . $presets_html . '</ul>';
    }
    else {
      $presets_html = '- ' . t('none') . ' -';
    }

    $rows[] = [
      '<strong>' . l($interfaceLabel, $interface_path) . '</strong>'
      . '<br/>'
      . '<code>' . $interface . '</code>',
      $presets_html,
      '[' . $add_preset_link . ']',
    ];
  }

  return [
    /* @see theme_table() */
    '#theme' => 'table',
    '#header' => [
      t('Type'),
      t('Stored presets'),
      t('Add preset'),
    ],
    '#rows' => $rows,
  ];
}
