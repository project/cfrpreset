<?php

namespace Drupal\cfrpreset\CfrPreset;

/**
 * Pseudo-interface to allow the IDE to recognize properties on cfrpreset
 * records. These records really are just instances of \stdClass, but phpdoc
 * will tell the IDE they could be instances of this interface.
 *
 * @property int $id
 *   Auto-increment id.
 * @property string $identifier
 *   $->identifier === $->interface  . '|' . $->machine_name
 * @property string $interface
 * @property string $machine_name
 * @property string $label
 * @property string $plugin_id
 * @property mixed $plugin_options
 *
 * @see cfrpreset_schema()
 */
interface CfrPresetInterface {

}
