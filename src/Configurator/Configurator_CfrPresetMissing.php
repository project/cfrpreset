<?php

namespace Drupal\cfrpreset\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Unconfigurable\Configurator_OptionlessBase;
use Drupal\cfrapi\Exception\ConfToValueException;

class Configurator_CfrPresetMissing extends Configurator_OptionlessBase {

  /**
   * @var string
   */
  private $interface;

  /**
   * @var string
   */
  private $presetMachineName;

  /**
   * @param string $interface
   * @param string $presetMachineName
   */
  public function __construct($interface, $presetMachineName) {
    $this->interface = $interface;
    $this->presetMachineName = $presetMachineName;
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   *
   * @return mixed
   *   Value to be used in the application.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function confGetValue($conf) {
    throw new ConfToValueException("Preset '$this->presetMachineName' for interface '$this->interface' does not exist.");
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *
   * @return string
   *   PHP statement to generate the value.
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    return $helper->incompatibleConfiguration(
      // @todo This is always NULL, so not very interesting.
      $conf,
      "Preset '$this->presetMachineName' for interface '$this->interface' does not exist.");
  }
}
