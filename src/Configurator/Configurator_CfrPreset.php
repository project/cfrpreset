<?php

namespace Drupal\cfrpreset\Configurator;

use Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface;
use Drupal\cfrapi\Configurator\Unconfigurable\Configurator_OptionlessBase;
use Drupal\cfrapi\Exception\ConfToValueException;

class Configurator_CfrPreset extends Configurator_OptionlessBase {

  /**
   * @var \Drupal\cfrpreset\CfrPreset\CfrPresetInterface|object
   */
  private $record;

  /**
   * @var bool
   */
  private $initialized = FALSE;

  /**
   * @var string|null
   */
  private $php;

  /**
   * @var mixed|null
   */
  private $value;

  /**
   * @var null|\Drupal\cfrapi\Exception\ConfToValueException
   */
  private $exception;

  /**
   * @param string $interface
   * @param string $presetMachineName
   * @param mixed $conf
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   *
   * @deprecated
   *   This method only exists for backwards compatibility with existing definitions.
   */
  public static function create($interface, $presetMachineName,
    /** @noinspection PhpUnusedParameterInspection */ $conf) {
    return self::load($interface, $presetMachineName);
  }

  /**
   * @param string $interface
   * @param string $presetMachineName
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  public static function load($interface, $presetMachineName) {

    if (NULL === $record = _cfrpreset_load_preset($interface, $presetMachineName)) {
      return new Configurator_CfrPresetMissing($interface, $presetMachineName);
    }

    return new self($record);
  }

  /**
   * @param string $interface
   * @param string $presetMachineName
   *
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface|null
   */
  public static function loadOrNull($interface, $presetMachineName) {

    if (NULL === $record = _cfrpreset_load_preset($interface, $presetMachineName)) {
      return NULL;
    }

    return new self($record);
  }

  /**
   * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
   */
  public function __construct($record) {
    $this->record = $record;
  }

  /**
   * @param mixed $conf_ignore
   *   Configuration from a form, config file or storage.
   * @param string|null $label
   *   Label for the form element, specifying the purpose where it is used.
   *
   * @return array
   */
  public function confGetForm($conf_ignore, $label) {

    if (!user_access('administer cfrpreset')) {
      return [];
    }

    $link = l(
      t('edit preset'),
      _cfrpreset_preset_get_path($this->record),
      ['attributes' => ['target' => '_blank']]);

    return [
      '#type' => 'container',
      'link' => [
        '#markup' => $link,
      ],
    ];
  }

  /**
   * @param mixed $conf_ignore
   *   Configuration from a form, config file or storage.
   *
   * @return mixed
   *   Value to be used in the application.
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  public function confGetValue($conf_ignore) {

    if ($this->initialized) {
      return $this->value;
    }

    if (null !== $this->exception) {
      throw $this->exception;
    }

    try {
      $this->value = $this->createValue();
    }
    catch (ConfToValueException $e) {
      $this->exception = $e;
      throw $e;
    }

    $this->initialized = TRUE;
    return $this->value;
  }

  /**
   * @param mixed $conf
   *   Configuration from a form, config file or storage.
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *
   * @return string
   *   PHP statement to generate the value.
   */
  public function confGetPhp($conf, CfrCodegenHelperInterface $helper) {

    return (NULL !== $this->php)
      ? $this->php
      : $this->php = $this->generatePhp($helper);
  }

  /**
   * @return mixed
   *
   * @throws \Drupal\cfrapi\Exception\ConfToValueException
   */
  private function createValue() {
    static $recursion_level = 0;
    if ($recursion_level > 12) {
      // @todo Return error value?
      return NULL;
    }
    $current_recursion_level = $recursion_level;
    ++$recursion_level;

    try {
      return $this->getTypeConfigurator()->confGetValue($this->getConf());
    }
    catch (\Exception $e) {

      watchdog('cfrplugin',
        'Broken preset !preset for %interface.'
        . "\n" . 'Exception message: %message',
        [
          '!preset' => l($this->record->label, _cfrpreset_preset_get_path($this->record)),
          '%interface' => $this->record->interface,
          '%message' => $e->getMessage(),
        ],
        WATCHDOG_WARNING);

      throw new ConfToValueException("Broken preset '{$this->record->machine_name}' for '{$this->record->interface}'.", NULL, $e);
    }
    // Let the exception slip through.
    finally {
      $recursion_level = $current_recursion_level;
    }
  }

  /**
   * @param \Drupal\cfrapi\CfrCodegenHelper\CfrCodegenHelperInterface $helper
   *
   * @return string
   */
  private function generatePhp(CfrCodegenHelperInterface $helper) {

    static $recursion_level = 0;
    if ($recursion_level > 12) {
      return $helper->recursionDetected(NULL, 'Recursion in cfrpreset.');
    }
    $current_recursion_level = $recursion_level;
    ++$recursion_level;

    $php = $this->getTypeConfigurator()->confGetPhp($this->getConf(), $helper);

    $recursion_level = $current_recursion_level;

    return '// Preset: ' . str_replace("\n", "\n// ", $this->record->machine_name)
      . "\n" . $php;
  }

  /**
   * @return \Drupal\cfrapi\Configurator\ConfiguratorInterface
   */
  private function getTypeConfigurator() {
    return cfrplugin()->interfaceGetConfigurator($this->record->interface);
  }

  /**
   * @return array
   */
  private function getConf() {
    return [
      'id' => $this->record->plugin_id,
      'options' => $this->record->plugin_options,
    ];
  }
}
