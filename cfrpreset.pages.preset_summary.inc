<?php
use Drupal\cfrapi\SummaryBuilder\SummaryBuilder_Static;

/**
 * Page callback.
 *
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return array|string
 */
function _cfrpreset_preset_summary_page($record) {
  $conf = _cfrpreset_preset_get_plugin_conf($record);
  $summary = cfrplugin()->interfaceGetConfigurator($record->interface)->confGetSummary($conf, new SummaryBuilder_Static());

  return $summary;
}

