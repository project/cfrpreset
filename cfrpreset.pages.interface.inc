<?php

/**
 * @param string $interface
 *
 * @return string|array
 */
function _cfrpreset_interface_page($interface) {
  $rows = [];
  $presets = _cfrpreset_interface_get_presets_by_machine_name($interface);
  foreach ($presets as $machine_name => $record) {
    $preset_path = _cfrpreset_preset_get_path($record);
    // @todo Introduce a shortcut for summary-building.
    $summary = _cfrpreset_preset_get_plugin_summary($record);
    $rows[] = [
      check_plain($record->label),
      l(t('edit'), $preset_path),
      l(t('delete'), $preset_path . '/delete'),
      $summary,
    ];
  }
  return [
    '#theme' => 'table',
    '#rows' => $rows,
  ];
}
