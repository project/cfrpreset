<?php

/**
 * Implements hook_menu().
 */
function cfrpreset_menu() {
  $items = [];
  $items['admin/structure/cfrpreset'] = [
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => ['administer cfrpreset'],
    'title' => 'Cfr plugin presets',
    /* @see _cfrpreset_overview_page() */
    'page callback' => '_cfrpreset_overview_page',
    'file' => 'cfrpreset.pages.overview.inc',
  ];
  /* @see _cfrpreset_interface_load() */
  $items['admin/structure/cfrpreset/%_cfrpreset_interface'] = [
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => ['administer cfrpreset'],
    /* @see _cfrpreset_interface_page_title() */
    'title callback' => '_cfrpreset_interface_page_title',
    'title arguments' => [3],
    /* @see _cfrpreset_interface_page() */
    'page callback' => '_cfrpreset_interface_page',
    'page arguments' => [3],
    'file' => 'cfrpreset.pages.interface.inc',
  ];
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/list'] = [
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'List',
    'weight' => -10,
  ];
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/code'] = [
    'type' => MENU_LOCAL_TASK,
    'access arguments' => ['administer cfrpreset'],
    'title' => 'View code',
    /* @see _cfrpreset_interface_code_page() */
    'page callback' => '_cfrpreset_interface_code_page',
    'page arguments' => [3],
    'file' => 'cfrpreset.pages.code.inc',
  ];
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/add'] = [
    'type' => MENU_LOCAL_TASK | MENU_LOCAL_ACTION,
    'access arguments' => ['administer cfrpreset'],
    'title' => 'Add preset',
    'page callback' => 'drupal_get_form',
    /* @see _cfrpreset_preset_form() */
    'page arguments' => ['_cfrpreset_preset_form', 3],
    'file' => 'cfrpreset.pages.preset_form.inc',
  ];
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/preset/%_cfrpreset_preset'] = [
    'type' => MENU_NORMAL_ITEM,
    'load arguments' => [3],
    'access arguments' => ['administer cfrpreset'],
    /* @see _cfrpreset_preset_page_title() */
    'title callback' => '_cfrpreset_preset_page_title',
    'title arguments' => [3, 5],
    'page callback' => 'drupal_get_form',
    /* @see _cfrpreset_preset_form() */
    'page arguments' => ['_cfrpreset_preset_form', 3, 5],
    'file' => 'cfrpreset.pages.preset_form.inc',
  ];
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/preset/%_cfrpreset_preset/summary'] = [
    'type' => MENU_LOCAL_TASK,
    'load arguments' => [3],
    'access arguments' => ['administer cfrpreset'],
    'title' => 'Summary',
    /* @see _cfrpreset_preset_summary_page() */
    'page callback' => '_cfrpreset_preset_summary_page',
    'page arguments' => [5],
    'file' => 'cfrpreset.pages.preset_summary.inc',
    'weight' => 5,
  ];
  if (module_exists('devel')) {
    $items['admin/structure/cfrpreset/%_cfrpreset_interface/preset/%_cfrpreset_preset/devel'] = [
      'type' => MENU_LOCAL_TASK,
      'load arguments' => [3],
      'access arguments' => ['administer cfrpreset'],
      'title' => 'Devel',
      /* @see _cfrpreset_preset_devel_page() */
      'page callback' => '_cfrpreset_preset_devel_page',
      'page arguments' => [5],
      'file' => 'cfrpreset.pages.preset_devel.inc',
      'weight' => 10,
    ];
  }
  if (TRUE) {
    $items['admin/structure/cfrpreset/%_cfrpreset_interface/preset/%_cfrpreset_preset/edit'] = [
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE | MENU_CONTEXT_PAGE,
      'title' => 'Edit',
      'weight' => -10,
    ];
  }
  $items['admin/structure/cfrpreset/%_cfrpreset_interface/preset/%_cfrpreset_preset/delete'] = [
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'load arguments' => [3],
    'access arguments' => ['administer cfrpreset'],
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    /* @see _cfrpreset_preset_delete_confirm_form() */
    'page arguments' => ['_cfrpreset_preset_delete_confirm_form', 5],
    'file' => 'cfrpreset.pages.preset_delete.inc',
  ];
  return $items;
}

/**
 * Menu wildcard loader for '%_cfrpreset_interface'
 *
 * @param string $arg
 *
 * @return mixed
 */
function _cfrpreset_interface_load($arg) {
  $interface = str_replace('.', '\\', $arg);
  if (!_cfrpreset_valid_interface_name($interface)) {
    return FALSE;
  }
  // At this point, $interface looks like a valid class name. But it could still
  // be a non-existing interface, and possibly something ridiculously long.
  // Avoid interface_exists(), because autoload can have side effects.
  return $interface;
}

/**
 * Menu wildcard loader for '%_cfrpreset_preset'
 *
 * @param string $arg
 * @param string $interface_arg
 *
 * @return object|false|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface
 */
function _cfrpreset_preset_load($arg, $interface_arg) {
  $interface = _cfrpreset_interface_load($interface_arg);
  if (FALSE === $interface) {
    return FALSE;
  }
  if (NULL === $preset = _cfrpreset_load_preset($interface, $arg)) {
    return FALSE;
  }
  return $preset;
}

/**
 * @param string $interface
 *
 * @return string
 */
function _cfrpreset_interface_page_title($interface) {
  return _cfrpreset_interface_get_title($interface);
}

/**
 * @param string $interface
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $preset
 *
 * @return string
 */
function _cfrpreset_preset_page_title(
  /** @noinspection PhpUnusedParameterInspection */ $interface,
  $preset
) {
  return $preset->label;
}

/**
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return string
 */
function _cfrpreset_preset_get_path($record) {
  return _cfrpreset_interface_get_path($record->interface) . '/preset/' . $record->machine_name;
}

/**
 * @param string $interface
 *
 * @return string
 */
function _cfrpreset_interface_get_path($interface) {
  return 'admin/structure/cfrpreset/' . str_replace('\\', '.', $interface);
}
