<?php
use Drupal\cfrreflection\Util\StringUtil;

/**
 * Form builder callback.
 *
 * @param array $form
 * @param array $form_state
 * @param string $interface
 * @param object|null|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return array
 */
function _cfrpreset_preset_form(array $form,
  /** @noinspection PhpUnusedParameterInspection */ array &$form_state, $interface, $record = NULL) {

  $form['#record'] = $record;

  $form['identifier'] = [
    '#type' => 'value',
    '#value' => NULL,
  ];
  $form['interface'] = [
    '#type' => 'value',
    '#value' => $interface,
  ];
  $form['label'] = [
    '#title' => t('Administrative title'),
    '#type' => 'textfield',
    '#required' => TRUE,
  ];

  // Placeholder.
  $form['machine_name'] = [];

  // Placeholder.
  $form['plugin'] = [];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    /* @see _cfrpreset_preset_form_submit() */
    '#submit' => ['_cfrpreset_preset_form_submit'],
  ];

  if (NULL !== $record) {

    $form['identifier']['#default_value'] = $record->identifier;
    $form['machine_name']['#default_value'] = $record->machine_name;
    $form['label']['#default_value'] = $record->label;

    $form['machine_name'] = [
      '#type' => 'value',
      '#value' => $record->machine_name,
    ];

    $conf = [
      'id' => $record->plugin_id,
      'options' => $record->plugin_options,
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      /* @see _cfrpreset_preset_form_delete_submit() */
      '#submit' => ['_cfrpreset_preset_form_delete_submit'],
    ];
  }
  else {

    drupal_set_title(
      t(
        'Create %type plugin preset',
        ['%type' => StringUtil::interfaceGenerateLabel($interface)]),
      PASS_THROUGH);

    $form['machine_name'] = [
      '#title' => t('Machine name'),
      /* @see form_process_machine_name() */
      /* @see form_validate_machine_name() */
      '#type' => 'machine_name',
      '#machine_name' => [
        'source' => ['label'],
        /* @see _cfrpreset_preset_form_machine_name_exists() */
        'exists' => '_cfrpreset_preset_form_machine_name_exists',
      ],
      // This is picked up by the 'exists' function.
      '#cfrpreset_interface' => $interface,
      '#required' => TRUE,
    ];

    if (!empty($_GET['plugin'])) {
      $conf = $_GET['plugin'];
    }
    else {
      $conf = NULL;
    }
  }

  $form['plugin'] = [
    '#title' => t('Plugin'),
    /* @see cfrplugin_element_info() */
    '#type' => 'cfrplugin',
    '#default_value' => $conf,
    '#cfrplugin_interface' => $interface,
    # '#cfrplugin_cfrpreset_export_label' => t('clone'),
  ];

  return $form;
}

/**
 * Form validation callback.
 *
 * @param array $form
 * @param array $form_state
 */
function _cfrpreset_preset_form_validate(array &$form, array &$form_state) {

  $values = $form_state['values'];
  if (!empty($form['#record'])) {
    /** @var object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record */
    $record = $form['#record'];
    // Check if record has changed in the database.
    if ([] !== _cfrpreset_preset_find_modifications($record)) {
      $link = l(t('new browser tab'), $_GET['q'], ['attributes' => ['target' => '_blank']]);
      form_set_error('',
        t('It appears that the preset was modified by someone else in the meantime.')
        . '<br/>' . t('You may want to open a new instance of this form in a !new_browser_tab, and sync your changes manually.', ['!new_browser_tab' => $link]));
      return;
    }
  }
  else {
    $record = _cfrpreset_create_stub($values['interface'], $values['machine_name']);
  }

  $record->label = $values['label'];
  $record->plugin_id = isset($values['plugin']['id']) ? $values['plugin']['id'] : NULL;
  $record->plugin_options = isset($values['plugin']['options']) ? $values['plugin']['options'] : NULL;

  $form_state['cfrpreset_preset_to_save'] = $record;
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function _cfrpreset_preset_form_submit(
  /** @noinspection PhpUnusedParameterInspection */ array &$form,
  array &$form_state
) {
  if (empty($form_state['cfrpreset_preset_to_save'])) {
    drupal_set_message('No record that could be saved.', 'error');
    return;
  }
  /** @var object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record */
  $record = $form_state['cfrpreset_preset_to_save'];
  $success = _cfrpreset_preset_save($record);
  if (FALSE === $success) {
    drupal_set_message('Failed to save the preset.', 'error');
  }
  elseif (SAVED_NEW === $success) {
    drupal_set_message('New preset created.');
    $form_state['redirect'] = _cfrpreset_preset_get_path($record);
  }
  elseif (SAVED_UPDATED === $success) {
    drupal_set_message('Preset updated.');
  }
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function _cfrpreset_preset_form_delete_submit(array &$form, array &$form_state) {
  $destination = [];
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $record = $form['#record'];
  $form_state['redirect'] = [_cfrpreset_preset_get_path($record) . '/delete', ['query' => $destination]];
}
