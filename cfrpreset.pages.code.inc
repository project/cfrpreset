<?php

/**
 * Page callback for admin/structure/cfrpreset/%/code
 *
 * @param string $interface
 *
 * @return string|array
 */
function _cfrpreset_interface_code_page($interface) {

  if (!interface_exists($interface) && !class_exists($interface)) {
    return t('There is no class or interface by this name.');
  }

  try {
    $reflectionClass = new ReflectionClass($interface);
  }
  catch (ReflectionException $e) {
    return t('The class or interface does not exist.');
  }

  if (FALSE === $filename = $reflectionClass->getFileName()) {
    return t('The class or interface has no class file.');
  }

  if (!is_readable($filename)) {
    return t('The file for this class or interface is not readable.');
  }

  $php = file_get_contents($filename);

  return _cfrpreset_highlight_php($php);
}

/**
 * @param string $class
 *
 * @return string|null
 */
function _cfrpreset_class_get_php($class) {

  try {
    $reflectionClass = new ReflectionClass($class);
  }
  catch (ReflectionException $e) {
    return NULL;
  }

  $filename = $reflectionClass->getFileName();

  if (FALSE === $filename || !is_readable($filename)) {
    return NULL;
  }

  return file_get_contents($filename);
}
