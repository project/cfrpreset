<?php

/**
 * Form builder callback.
 *
 * @param array $form
 * @param array $form_state
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return array
 */
function _cfrpreset_preset_delete_confirm_form(
  array $form,
  /** @noinspection PhpUnusedParameterInspection */ array &$form_state,
  $record
) {
  $form['#record'] = $record;

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title?', ['%title' => $record->label]),
    _cfrpreset_preset_get_path($record),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

/**
 * @param array $form
 * @param array $form_state
 */
function _cfrpreset_preset_delete_confirm_form_validate(
  array $form,
  /** @noinspection PhpUnusedParameterInspection */ array &$form_state
) {
  /** @var object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record */
  $record = $form['#record'];

  if ([] !== _cfrpreset_preset_find_modifications($record)) {
    form_set_error(NULL, t('It appears that the preset was modified by someone else.'));
    return;
  }
}

/**
 * Form submit callback.
 *
 * @param array $form
 * @param array $form_state
 */
function _cfrpreset_preset_delete_confirm_form_submit(array $form, array &$form_state) {

  /** @var object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record */
  $record = $form['#record'];

  if (!$form_state['values']['confirm']) {
    $form_state['redirect'] = _cfrpreset_preset_get_path($record);
    return;
  }

  _cfrpreset_preset_export_delete($record);
  watchdog('cfrpreset', '@type: deleted %title.', ['@type' => $record->interface, '%title' => $record->label]);
  drupal_set_message(t('@type %title has been deleted.', ['@type' => $record->interface, '%title' => $record->label]));

  $form_state['redirect'] = _cfrpreset_interface_get_path($record->interface);
}
