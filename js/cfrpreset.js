
(function($) {

  function extractValues(formValuesAll, prefix_search, prefix_replace) {

    var values = {};
    for (var i = 0; i < formValuesAll.length; ++i) {
      var vv = formValuesAll[i];
      if (prefix_search !== vv.name.substr(0, prefix_search.length)) {
        continue;
      }
      var localname = prefix_replace + vv.name.substr(prefix_search.length);
      values[localname] = vv.value;
    }

    return values;
  }

  Drupal.behaviors.cfrpreset = {
    attach: function (context) {
      $('.cfrplugin-tools', context).each(function() {
        var $tools = $(this);
        var $drilldown = $tools.parents('.cfr-drilldown').first();
        var $select = $('> * > select.cfr-drilldown-select, > * > * select.cfr-drilldown-select', $drilldown).first();
        var $presetLink = $('a.cfrplugin-cfrpreset-link', $tools);
        var $form = $select.parents('form').first();
        var selectName = $select.attr('name');
        if (0
          || 1 !== $drilldown.length
          || 1 !== $select.length
          || 1 !== $presetLink.length
          || 1 !== $form.length
          || '[id]' !== selectName.substr(-4)
        ) {
          return;
        }

        var name = selectName.slice(0, -4);

        var f = function() {
          var formValuesAll = $form.serializeArray();
          var values = extractValues(formValuesAll, name + '[options]', 'plugin[options]');
          values['plugin[id]'] = $select.val();
          $presetLink[0].search = '?' + $.param(values);
        };

        $presetLink.mousedown(f);
        $presetLink.click(f);
      });
    }
  };
})(jQuery);
