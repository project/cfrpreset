<?php

/**
 * Page callback.
 * @param object|\Drupal\cfrpreset\CfrPreset\CfrPresetInterface $record
 *
 * @return array|string
 */
function _cfrpreset_preset_devel_page($record) {
  $conf = _cfrpreset_preset_get_plugin_conf($record);

  $values_to_dump = ['conf' => $conf];

  try {
    $values_to_dump['handler'] = cfrplugin()->interfaceGetConfigurator($record->interface)->confGetValue($conf);
  }
  catch (\Exception $e) {
    $values_to_dump['exception'] = $e;
  }

  $dumped = [];
  if (function_exists('Drupal\krumong\dpm')) {
    foreach ($values_to_dump as $k => $v) {
      $dumped[$k] = krumong()->dump($v);
    }
  }
  elseif (function_exists('kdevel_print_object')) {
    foreach ($values_to_dump as $k => $v) {
      $dumped[$k] = kdevel_print_object($v);
    }
  }
  else {
    return t('Devel is not installed.');
  }

  $output = '';
  foreach ([
    'conf' => t('Configuration'),
    'handler'=> t('Handler'),
    'exception' => t('Exception'),
  ] as $key => $label) {
    if (!isset($dumped[$key])) {
      continue;
    }
    $output .= '<h2>' . $label . '</h2>';
    $output .= $dumped[$key];
  }

  return $output;
}
